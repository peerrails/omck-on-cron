require "rubygems"
require "sequel"
require "pg"
require 'net/http'
require "json"
#require "nokogiri"
require 'uri'

#sudo docker run --name omckoncron -d -e PG_ENV_POSTGRES_DATABASE=omckonrails -e PG_ENV_POSTGRES_PASSWORD=Skur114 -e PG_PORT_5432_TCP_ADDR=172.17.42.1 -e PG_ENV_POSTGRES_USER=production -e TWITCH_ID=$TWITCH_ID cronjob

PG_HOST = ENV["PG_PORT_5432_TCP_ADDR"] || "localhost"
PG_DATABASE = ENV["PG_ENV_POSTGRES_DATABASE"] || "omckonrails-dev"
PG_USER = ENV["PG_ENV_POSTGRES_USER"] || "dev"
PG_PASS = ENV["PG_ENV_POSTGRES_PASSWORD"] || "dev"

DB = Sequel.postgres(PG_DATABASE, :host=>PG_HOST, :user=>PG_USER, :password=>PG_PASS)


module Parser

  def run_job
    check_twitches
    puts_live
  end

  def get_twitch_streams(streams=["omcktv"])
    if streams != []
      channels = streams.join(',')
      response_body = '{"streams": []}'
      5.times do
        begin
          uri = URI.parse("https://api.twitch.tv/kraken/streams?channel=#{channels}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          req = Net::HTTP::Get.new(uri)
          req["CLIENT-ID"] = ENV["TWITCH_ID"]
          response_body = http.request(req).read_body
          break
        rescue => e
          puts e
        end
      end
      return JSON.parse(response_body)
    else
      return {"streams" => []}
    end
  end

  def check_twitches
    twitches = []
    lives = []
    #fetch twitch streams
    DB[:channels].select(:channel).where(:service => "twitch").each do |channel|
      twitches << channel[:channel]
    end
    begin
      live_streams = get_twitch_streams(twitches)
      unless live_streams["streams"] == []
        live_streams["streams"].each do |chan|
          DB[:channels]
            .where(:channel => chan["channel"]["name"])
            .update(
              game: chan["channel"]["game"],
              viewers: chan["viewers"],
              title: chan["channel"]["status"],
              live: true
          )
          lives << chan["channel"]["name"]
        end
      else
        live_streams = []
      end
    rescue => e
      puts e
      puts "Twitch error!"
      live_streams = []
    end
    twitches.each do |chan|
      unless lives.include? chan
        DB[:channels]
          .where(:channel => chan)
          .where(:live => true)
          .update(viewers: 0, live: false)
      end
    end
  end

  def puts_live
    DB[:channels].select(:channel).where(:live => true).each do |c|
      puts c[:channel]
    end
  end
end
